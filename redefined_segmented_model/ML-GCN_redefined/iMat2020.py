import torch.utils.data as data
import json
import os
import subprocess
from PIL import Image, ImageOps
import numpy as np
import torch
import pickle
from util import *
import time


class iMat2020(data.Dataset):
    def __init__(self, root, transform=None, phase='train', inp_name=None):
        self.root = root
        self.phase = phase
        self.img_list = []
        self.transform = transform
        # download_coco2014(root, phase)
        self.get_anno()
        self.num_classes = len(self.cat2idx)

        with open(inp_name, 'rb') as f:
            self.inp = pickle.load(f)
        self.inp_name = inp_name

    def get_anno(self):
        list_path = os.path.join(self.root, 'data/iMat/', 'seg_{}_anno_2020.json'.format(self.phase))
        self.img_list = json.load(open(list_path, 'r'))
        self.img_list = self.img_list
        self.cat2idx = json.load(open(os.path.join(self.root, 'data/iMat/', 'category.json'), 'r'))


    def __len__(self):
        return len(self.img_list)

    def __getitem__(self, index):
        item = self.img_list[index]
        return self.get(item)


    def rle_decode(self,mask_rle, shape):
        s = mask_rle
        starts, lengths = [np.asarray(x, dtype=int) for x in (s[0:][::2], s[1:][::2])]
        starts -= 1
        ends = starts + lengths
        img = np.zeros(shape[0]*shape[1], dtype=np.uint8)
        for lo, hi in zip(starts, ends):
            img[lo:hi] = 1
        return img.reshape(shape)


    def get(self, item):
        
        labels = sorted(item['labels'])
        filename = item['file_name']
        # print("file_name", filename)
        im1 = Image.open(os.path.join('/media/chintu/bharath_ext_hdd/Bharath/Attribute Classifier/train', '{}.jpg'.format(filename.split('_')[1]))).convert('RGB')
        pixels = item['EncodedPixels'].split()
        IMG_SHAPE=(int(item['Width']),int(item['Height']))
        
        all_masks = np.zeros(IMG_SHAPE)
        all_masks += self.rle_decode(pixels, shape=IMG_SHAPE)
        # mask1 = np.logical_not(all_masks.T).astype(int)*256


        # im1 = Image.open(img_path).convert('RGB')
        # pixels = encoded_pixels.split()
        # IMG_SHAPE=(int(im1.size[0]),int(im1.size[1]))

        # all_masks = np.zeros(IMG_SHAPE)
        # all_masks += rle_decode(pixels, shape=IMG_SHAPE)

        #crop orig image from mask

        #SINGLE ENCODED PIXEL causes 0 w,h issue for new image from mask
        try:

            yy, xx = np.where(all_masks.T)
            img = np.array(im1)
            img[all_masks.T==0]=0
            
            xx_min, xx_max = xx.min(), xx.max()
            yy_min, yy_max = yy.min(), yy.max()

            if xx_min==xx_max:
                xx_max= xx_min+2

            if yy_min==yy_max:
                yy_max= yy_min+2

            img1 = Image.fromarray(np.uint8(img)).crop([xx_min,yy_min,xx_max,yy_max])

            org_w, org_h = img1.size[0], img1.size[1]
            img2 = ImageOps.expand(img1,border=100,fill='black')
            
            img3 = img2.resize((org_w, org_h),Image.BILINEAR)
            mask= np.array(img3.resize(img3.size).convert('L'))
            mask = Image.fromarray(np.uint8(mask))
            img3.putalpha(mask)

            # img = mask1[:,:,None]+np.array(im1)
            # img[img>=256]=255
            # img = Image.fromarray(np.uint8(img))

            if self.transform is not None:
                img = self.transform(img3)
            target = np.zeros(self.num_classes, np.float32) -1
            target[labels] = 1
            del im1, pixels, all_masks, mask
            
            return (img, filename, self.inp), target
        
        except:
            print("org_w-h",filename,im1.size)
            print("img1", img1.size)
            pass

        



class load_testdata(data.Dataset):
    def __init__(self, root, transform=None, inp_name=None):
        self.root = root
        print(self.root)
        self.img_list = []
        self.transform = transform
        self.get_anno()
        self.num_classes = len(self.cat2idx)

        with open(inp_name, 'rb') as f:
            self.inp = pickle.load(f)
        self.inp_name = inp_name

    def get_anno(self):
        # list_path = os.path.join(self.root, 'data/iMat/', '{}_anno_2020.json'.format(self.phase))
        # self.img_list = json.load(open(list_path, 'r'))
        self.img_list = os.listdir(self.root)
        self.cat2idx = json.load(open(os.path.join('./data/iMat/', 'category.json'), 'r'))


    def __len__(self):
        return len(self.img_list)

    def __getitem__(self, index):
        item = self.img_list[index]
        return self.get(item)

    def get(self, item):
        filename = item
        img = Image.open(os.path.join(self.root, '{}'.format(filename)))#.convert('RGB')
        # print("img", img.size, np.array(img).shape)
        if self.transform is not None:
            img = self.transform(img)
        return (img, filename, self.inp)