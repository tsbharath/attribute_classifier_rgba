import torchvision.models as models
from torch.nn import Parameter
from util import *
import torch
import torch.nn as nn

class GraphConvolution(nn.Module):
    """
    Simple GCN layer, similar to https://arxiv.org/abs/1609.02907
    """

    def __init__(self, in_features, out_features, bias=False):
        super(GraphConvolution, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(in_features, out_features))
        # print("in, out, weight", in_features, out_features, self.weight.shape)
        if bias:
            self.bias = Parameter(torch.Tensor(1, 1, out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input, adj):
        support = torch.matmul(input, self.weight)
        output = torch.matmul(adj, support)
        if self.bias is not None:
            return output + self.bias
        else:
            return output

    def __repr__(self):
        return self.__class__.__name__ + ' (' \
               + str(self.in_features) + ' -> ' \
               + str(self.out_features) + ')'


class GCNResnet(nn.Module):
    def __init__(self, model, num_classes, in_channel=300, t=0, adj_file=None):
        super(GCNResnet, self).__init__()
        # print("model.conv1",model.conv1)
        
        # w = model.conv1.weight
        # print('w1',w.shape)
        # w = torch.cat([w, torch.full((64, 1, 7, 7), 0.01)], dim=1)
        # print('w',w.shape)
        # model.conv1.weight = nn.Parameter(w)
        # print("inside torch model.conv1", model.conv1.shape)
          
        # print("model.conv1",model.conv1)
        self.features = nn.Sequential(
            model.conv1, 
            model.bn1,
            model.relu,
            model.maxpool,
            model.layer1,
            model.layer2,
            model.layer3,
            model.layer4,
        )
        self.num_classes = num_classes
        self.pooling = nn.MaxPool2d(14, 14)

        self.gc1 = GraphConvolution(in_channel, 1024)
        self.gc2 = GraphConvolution(1024, 2048)
        self.relu = nn.LeakyReLU(0.2)

        _adj = gen_A(num_classes, t, adj_file)
        # print("adj",type(_adj))
        self.A = Parameter(torch.from_numpy(_adj).float())
        # image normalization
        self.image_normalization_mean = [0.485, 0.456, 0.406, 0.4]
        self.image_normalization_std = [0.229, 0.224, 0.225, 0.225 ]

    def forward(self, feature, inp):
        # print('initial feature', feature.shape)
        feature = self.features(feature)
        # print('features', feature.shape)
        feature = self.pooling(feature)
        feature = feature.view(feature.size(0), -1)
        # print('feature view', feature.shape)
        
        # print('inp', inp.shape)
        inp = inp[0]
        # print('inp[0]', inp.shape)
        adj = gen_adj(self.A).detach()
        # print("inp-adj",inp.shape, adj.shape)
        x = self.gc1(inp, adj)
        # print("gcn1-x", x.shape)
        x = self.relu(x)
        # print("x-adj", x.shape, adj.shape)
        x = self.gc2(x, adj)
        # print("gcn2-x", x.shape)
        gcn = x
        # print('x b4 transpose', x.shape)
        x = x.transpose(0, 1)
        # print("feature - x", feature.shape, x.shape)
        x = torch.matmul(feature, x)
        # print('final x', x.shape)
        return x , feature, gcn

    def get_config_optim(self, lr, lrp):
        # print('params', self.features.parameters(), 'lr', lr * lrp,
        #         'params', self.gc1.parameters(), 'lr', lr,
        #         'params', self.gc2.parameters(), 'lr', lr)
        return [
                {'params': self.features.parameters(), 'lr': lr * lrp},
                {'params': self.gc1.parameters(), 'lr': lr},
                {'params': self.gc2.parameters(), 'lr': lr},
                ]



def gcn_resnet101(num_classes, t, pretrained=True, adj_file=None, in_channel=300):
    model = models.resnet101(pretrained=pretrained)
    with torch.no_grad():
        w = model.conv1.weight
        # print('w1',w.shape)
        w = torch.cat([w, torch.full((64, 1, 7, 7), 0.01)], dim=1)
        # print('w',w.shape)
        model.conv1.weight = nn.Parameter(w)

    # print("inside torch model.conv1", model.conv1.weight )
    return GCNResnet(model, num_classes, t=t, adj_file=adj_file, in_channel=in_channel)
